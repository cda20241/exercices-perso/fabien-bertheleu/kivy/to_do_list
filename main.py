from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.checkbox import CheckBox
from kivy.uix.image import Image
from kivy.graphics import Rectangle
from kivy.core.image import Image as CoreImage


class MyApp(App):

    def build(self):
        #Creation des grids
        self.box_racine=GridLayout()
        self.box_racine.cols=1
        self.grid_input=GridLayout(cols=1)
        self.grid_liste=GridLayout(cols=2)
        self.grid_boutons=GridLayout(rows=2)

        self.to_do_list = []
        self.list_check = []

        #Ajout des boutons et frame du Grid Boutons
        self.grid_boutons.size_hint_y = 0.2
        # self.grid_boutons.spacing = 10
        self.grid_boutons.padding = 10
        btn_ajouter = Button(text="Ajouter", font_size=30, background_color="green")
        btn_ajouter.bind(on_press=self.bouton_ajouter)
        self.grid_boutons.add_widget(btn_ajouter)
        # btn_supprimer = Button(text="Supprimer", font_size=30, background_color="red")
        # btn_supprimer.bind(on_press=self.bouton_supprimer)
        # self.grid_boutons.add_widget(btn_supprimer)

        # Ajout des boutons et frame du Grid Input
        self.grid_input.padding=20
        label_principal = Label(text="Votre To-Do List", font_size=20)
        label_principal.background= Image(source = "lagon.jpg")
        self.grid_input.add_widget(label_principal)
        self.grid_input.size_hint_y = 0.4
        self.input = TextInput()
        self.input.bind(text=self.texte_input)
        self.grid_input.add_widget(self.input)

        #Ajout de Label à la grid List
        self.label_test = Label(text="Votre Liste Ici")
        self.grid_liste.add_widget(self.label_test)

        self.box_racine.add_widget(self.grid_input)

        self.box_racine.add_widget((self.grid_boutons))
        self.box_racine.add_widget(self.grid_liste)
        return self.box_racine

    def bouton_ajouter(self, instance):
        texte_input = self.input.text
        self.to_do_list.append(texte_input)
        self.grid_liste.clear_widgets()
        for elem_de_la_liste in self.to_do_list:
            #creation d'une nouvelle Grid
            nouvelle_grid=GridLayout(cols=2)
            # Utiliser chaque élément de la liste directement
            self.label_liste = Label(text=elem_de_la_liste, halign="left")
            #Ajout du nouveau label et de la checkbox à la nouvelle Grid
            self.bouton_valider()
            nouvelle_grid.add_widget(self.label_liste)
            #ajout de la nouvelle Grid à la Grid_Liste
            self.grid_liste.add_widget(nouvelle_grid)

        #Remise à vide de l'input
        self.input.text=""



    def bouton_supprimer(self, text):

        self.to_do_list.remove("ajout texte")
        self.grid_liste.remove_widget(self.label_test)




    def texte_input(self, instance, *args):
        self.label_test.text = instance.text
        # self.to_do_list.append(instance.text)

    def bouton_valider(self):
        self.btn_valider = Button(text="To do", background_color="red")
        self.btn_valider.bind(on_press=self.changeCouleur)
        self.btn_valider.bind(on_press=self.texte_btn_todo)
        self.grid_liste.add_widget(self.btn_valider)

    def changeCouleur(self, Button):
        self.btn_valider.background_color = "green"

    def texte_btn_todo(self, Button):
        self.btn_valider.text = "Done"






if __name__ == "__main__":
    MyApp().run()