# bouton de sauvegarde
btn_sauvegarder = Button(text="Sauvegarder", font_size=30, background_color=VERTFLASH)
btn_sauvegarder.bind(on_press=self.sauvegarder_dans_json)
self.grid_boutons.add_widget(btn_sauvegarder)



def sauvegarder_dans_json(self, instance):
    print("Tentative de sauvegarde!")
    key = "Youyou"
    data = self.to_do_store.get(key)
    print("Tentative de sauvegarde2!")
    buttons_info = []

    for grid in self.liste_essai:
        btn_tache_text = self.grid.children[0].text
        btn_valider_text = self.grid.children[1].text
        btn_supprimer_text = self.grid.children[2].text

        button_info = {
            "btn_tache": btn_tache_text,
            "btn_valider": btn_valider_text,
            "btn_supprimer": btn_supprimer_text
        }

        buttons_info.append(button_info)

    if data:
        existing_data = json.loads(data)
        existing_data.extend(buttons_info)
        self.to_do_store.put(key, data=json.dumps(existing_data))
    else:
        self.to_do_store.put(key, data=json.dumps(buttons_info))

    print("Données sauvegardées avec succès!")
