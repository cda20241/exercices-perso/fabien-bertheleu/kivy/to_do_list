def add(num1,num2):
    return num1 + num2

add(4,3)

print(add(4,3))
# (lambda num1, num2: num1 + num2)(3, 7)
add(4,3)
print(lambda num1, num2: num1 + num2)
