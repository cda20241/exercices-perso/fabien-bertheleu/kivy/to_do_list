from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.storage.jsonstore import JsonStore
import json


####  Création de différentes couleurs
VERTFLASH=(0,1.5,0.5,1)
FOND=(4,1.7,0.3,0)
ROUGEATTENUE=(3,0,0,.7)
VERTATTENUE=(0,1,1,0.8)
FONDTRANSPARENT=(1,1,1,0.5)
COULEURSUPPRIMER=(4,1,1,0.5)

class MyApp4(App):

    def build(self):
        ####  Image de fond
        self.Fond = FloatLayout()
        self.Img1 = Image(source="lagon.jpg", allow_stretch=True, keep_ratio=False, nocache=True)
        self.Fond.add_widget(self.Img1)

        ####  Creation des principaux Layout
        self.box_racine = GridLayout(cols=1)
        self.grid_input = GridLayout(cols=1)
        self.grid_liste = GridLayout(cols=1)
        self.grid_boutons = GridLayout(rows=2)

        self.to_do_list = []
        self.liste_essai = []

        ####Serialisation
        self.to_do_store = JsonStore('to_do.json')






        ####   Paramètres et Ajout des Boutons de la  grid_boutons
        self.grid_boutons.size_hint_y = 0.2
        self.grid_boutons.padding = 10
        btn_ajouter = Button(text="Ajouter", font_size=30, background_color=VERTFLASH)
        btn_ajouter.bind(on_press=self.bouton_ajouter)
        self.grid_boutons.add_widget(btn_ajouter)

        ####   Paramètres et Ajout des Boutons de la  grid_input
        label_principal = Label(text="Votre To-Do List", font_size=40, bold=True)
        self.grid_input.add_widget(label_principal)
        self.grid_input.padding = 20
        self.grid_input.size_hint_y = 0.4
        self.input = TextInput()
        self.input.bind(text=self.texte_input)
        self.grid_input.add_widget(self.input)


        #### Mise en place des grids sur la grid principale
        self.Fond.add_widget(self.box_racine)
        self.box_racine.add_widget(self.grid_input)
        self.box_racine.add_widget(self.grid_boutons)
        self.box_racine.add_widget(self.grid_liste)

        return self.Fond

    """
    Fonction du bond du bouton ajouter, qui permet de créer une nouvelle grid qui s'insinue dans sa 
        propre grid parent
    """
    def bouton_ajouter(self, instance):
        texte_input = self.input.text
        self.to_do_list.append(texte_input)
        self.grid_liste.clear_widgets()


        ############################################## ON REFAIT MAIS EN BOUCLE ################################
        for elem_de_la_liste in self.to_do_list:
            # creation d'une nouvelle Grid
            nouvelle_grid = GridLayout(cols=3)
            # Creation du bouton qui affiche le texte de l'input
            btn_tache = Button(text=elem_de_la_liste, background_color=FONDTRANSPARENT, bold=True)
            nouvelle_grid.add_widget(btn_tache)

            # Utiliser chaque élément de la liste directement
            # self.label_liste = Label(text=elem_de_la_liste, halign="left")
            # Ajout du nouveau label et de la checkbox à la nouvelle Grid
            self.to_do_store.put(elem_de_la_liste, Done=False)
            self.bouton_valider(nouvelle_grid, elem_de_la_liste)
            self.bouton_supprimer(nouvelle_grid)
            # nouvelle_grid.add_widget(self.label_liste)
            # ajout de la nouvelle Grid à la Grid_Liste
            self.grid_liste.add_widget(nouvelle_grid)

            for widget in nouvelle_grid.children:
                if isinstance(widget, Button):
                    print(f"texte du bouton : {widget.text}")
                    # self.to_do_list.put(elem_de_la_liste, data=f"{widget.text}")
            # for widget in nouvelle_grid.children:





        ########################################## ANCIEN CODE ##################################################
        # # Creation du bouton qui affiche le texte de l'input
        # btn_tache=Button(text=texte_input, background_color=FONDTRANSPARENT, bold=True)
        # nouvelle_grid.add_widget(btn_tache)
        # self.grid_liste.add_widget(nouvelle_grid)
        # # Creation des deux autres boutons de la nouvelle grif enfant
        # self.bouton_valider(nouvelle_grid)
        # self.bouton_supprimer(nouvelle_grid)

        # ZONE D'ESSAI SERIALIZATION
        self.liste_essai.append(nouvelle_grid)
        print(nouvelle_grid.children)

        # for widget in nouvelle_grid.children:
        #     if isinstance(widget, Button):
        #         print(f"{widget.text[0]} : {widget.text}")

        # self.to_do_store.put("ghj", data=f"{widget.text[0]} : {widget.text}")

        # # Conversion de la liste en chaîne JSON avant de la stocker
        # self.to_do_store.put("Youyou", data=self.to_do_list, )




        # Mise à zero de la zone d'input
        self.input.text = ""

    def texte_input(self, instance, *args):
        pass

    """
    Fonction qui créé le bouton valider avec ses dynamismes (changement de texte et de couleur)
    """
    def bouton_valider(self, nouvelle_grid, nom_tache):
        boolean_etat = self.get_etat_tache(nom_tache)
        texte_bouton=""
        if boolean_etat == False:
            texte_bouton="To do"
        else:
            texte_bouton="Done"

        btn_valider = Button(text=texte_bouton, background_color=ROUGEATTENUE, bold=True)
        btn_valider.nom_tache = nom_tache
        btn_valider.bind(on_press=lambda instance: self.changeCouleur(instance, nouvelle_grid))
        btn_valider.bind(on_press=lambda instance: self.bouton_done(instance, nouvelle_grid))
        nouvelle_grid.add_widget(btn_valider)
        return  boolean_etat

    def get_etat_tache(self, nom_tache):
        etat= self.to_do_store.get(nom_tache)
        return etat["Done"]
    

    """
    Fonction qui change la couleur d'un bouton
    """
    def changeCouleur(self, button, nouvelle_grid):
        button.background_color = VERTATTENUE

    """
    Fonction qui change le texte d'un bouton de validation
    """
    def bouton_done(self, button, nouvelle_grid):
        button.text = "Done"
        self.to_do_store.put(button.nom_tache, Done=True)

    """
    Fonction du bouton supprimer qui doit supprimer sa propre grid d'affichage
    """
    def bouton_supprimer(self, nouvelle_grid):
        btn_supprimer = Button(text="Supprimer", background_color=COULEURSUPPRIMER , bold=True)
        btn_supprimer.bind(on_press=lambda instance: self.fonction_supprimer(instance, nouvelle_grid))
        nouvelle_grid.add_widget(btn_supprimer)

    """
    Fonction de bind du bouton supprimer, qui sert à supprimer sa propre grid de la grid parent
    """
    def fonction_supprimer(self, button, nouvelle_grid):
        self.grid_liste.remove_widget(nouvelle_grid)


"""
Lancment de l'application
"""
if __name__ == "__main__":
    MyApp4().run()
